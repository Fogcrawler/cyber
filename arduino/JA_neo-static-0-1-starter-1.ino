// RGBW (Red Green Blue White Neo-Pixel starter code
// 16 LEDS with no loop inside of void loop()
// CW Coleman 180417

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6

#define NUM_LEDS 16

#define BRIGHTNESS 50

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  strip.setBrightness(BRIGHTNESS);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

// Initialize some variables for the void loop()
int led, red, green, blue, white;
int wait = 150;

void loop() {
// turn on led 0 - - -
  led = 0; 
  red = 255; 
  green = 0; 
  blue = 0; 
  white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();
  
// turn on the next led
  led = led +1 ; red = 255; green = 149; blue = 0; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 251; green = 255; blue = 0; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 255; blue = 0; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 174; blue = 255; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 170; green = 0; blue = 255; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 0; blue = 0; white = 255; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 0; blue = 255; white = 255; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ;  red = 255;   green = 0;   blue = 0;   white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();
  
  led = led +1 ; red = 255; green = 149; blue = 0; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 251; green = 255; blue = 0; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 255; blue = 0; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 174; blue = 255; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 170; green = 0; blue = 255; white = 10; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 0; blue = 0; white = 255; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  led = led +1 ; red = 0; green = 0; blue = 255; white = 255; 
  strip.setPixelColor(led, red, green , blue, white);
  delay(wait);
  strip.show();

  

}
